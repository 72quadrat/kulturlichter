---
Title: KULTURLICHTER - Preis des Bundes
---
Der Preis des Bundes zeichnet ein Projekt aus, das bundesweit adaptiert werden kann. Diese Auszeichnung ist mit 20.000 Euro dotiert.