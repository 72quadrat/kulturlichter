---
Title: KULTURLICHTER - Preis der Länder
---
Der Preis der Länder würdigt ein Projekt, das regional oder interregional übertragen werden kann. Diese Auszeichnung ist mit 20.000 Euro dotiert.