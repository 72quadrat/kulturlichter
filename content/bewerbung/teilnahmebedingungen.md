---
title: Wettbewerbsinformationen
Template: page
---

# Weitere Informationen zur Bewerbung

## Wer kann sich bewerben?

Am Wettbewerb können teilnehmen alle gemeinnützigen Kultureinrichtungen und -initiativen aus den Bereichen Musik, Theater, bildende, darstellende und angewandte Kunst, Literatur, Soziokultur, Film, Medien, Digitales und verwandte Formen sowie kulturgutbewahrende Einrichtungen nach §2 KGSG und Körperschaften des öffentlichen Rechts. Privatpersonen sind von der Teilnahme ausgeschlossen. Voraussetzung ist, dass sich jede Einrichtung oder Initiative mit einem konkreten Projekt bewirbt, wobei je Einrichtung/Projektträger pro Jahr nur ein Projekt zugelassen wird. Es kann entweder als Konzept vorliegen oder sich bereits in der Umsetzung befinden, muss die Effekte und Wirkungen bei den Zielgruppen plausibel belegen oder in Aussicht stellen können. Als Kooperationspartner können Einrichtungen der kulturellen Kinder-, Jugend- und Erwachsenenbildung wie auch Bildungseinrichtungen mitwirken.

Alle Projekte und ihre Beteiligten bekennen sich zu den freiheitlich-demokratischen Grundwerten, unterstützen die Thesen der Initiative kulturelle Integration und orientieren sich an den Zielen zur Entwicklung der kulturellen Bildung der UNESCO wie auch an den Zielen zur Bildung für nachhaltige Entwicklung der UNESCO.

## Nach welchen Kriterien wird der Preis vergeben?

Die Jury trifft die fachliche Auswahl für den Preis des Bundes und den Preis der Länder. Die Jury benennt auch die Nominierten für die Wahl des Publikumspreises. Diese Kriterien sind dabei entscheidend:

### digital			
Das Projekt nutzt innovative Methoden der Vermittlung und setzt neue Technologien – wie zum Beispiel künstliche Intelligenz – in der digital unterstützten kulturellen Bildung ein.

### reflektiert 		
Das Projekt reflektiert seine Vorannahmen und Voraussetzungen wie auch die Funktionalitäten der eingesetzten Mittel. Es macht plausibel, warum welche Instrumente in bestimmter Weise eingesetzt werden.

### beispielhaft 		
Das Projekt basiert auf einer originellen Idee. Es gibt anderen Einrichtungen und Akteuren Anregungen für die eigene kulturelle Bildung und Vermittlungspraxis.

### anschlussfähig		
Die im Projekt entwickelten Methoden und Instrumente sind veränderbar: Inhalte und Funktionen können also erweitert und in andere Kontexte übertragen werden. Der Projektträger und die Projektbeteiligten sollten bereit sein, Ideen und technische Entwicklungen mit anderen kostenfrei zu teilen.

### wirksam			
Dass das Projekt bei seinen Zielgruppen wirkt, ist bereits nachgewiesen oder kann plausibel in Aussicht gestellt werden.

### bildend
Das Projekt steht für Qualität in der Vermittlung und in der künstlerischen Gestaltung. Es nutzt technische Innovationen, um Bildungserfahrungen zu erweitern und kreative Ausdrucksformen zu ermöglichen. Es trägt damit zu einem besseren und breiteren Verständnis für das kulturelle Erbe oder auch die zeitgenössische Kunst und Kultur bei.

### integrativ und divers
Das Projekt fördert das Verständnis dafür, dass kulturelle und künstlerische Ausdrucksformen, Identitäten, Geschichtsauffassungen und Traditionen divers sind. Es bringt Menschen unterschiedlicher Herkunft zusammen und setzt sich für gesellschaftlichen Zusammenhalt ein.


## Informationen zur Bewerbung

Wettbewerbsbeiträge können vom 3. August bis zum 16. Oktober 2020 ausschließlich über das Online-Formular eingereicht werden. Postalische Einsendungen werden nicht berücksichtigt.

Über das Online-Formular werden folgende Dokumente und Angaben abgefragt:

- das Projektdesign einschließlich einer detaillierten Darstellung
der angewendeten Methoden, Instrumente und Technologien
sowie die Beschreibung der geplanten Umsetzung und des Transfers
als pdf-Datei
- Link zu Bildern und Videos aus dem Projekt; bei digitalen
Anwendungen Link zu einem Demonstrator
- Zeit- und Kostenplan zur Verwendung des Preisgeldes i.H.v.
20.000,– Euro
- Nachweis der Gemeinnützigkeit des Projektträgers oder der Initiative

## Kontakt

Kulturstiftung der Länder<br>
Stabsstelle Kulturelle Bildung<br>
E-Mail: <kontakt@kulturlichter-preis.de>

---

Alle Informationen zum Download finden Sie [hier](%assets_url%/Kulturlichter_InfoPDF.pdf).
