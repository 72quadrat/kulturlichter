Bewerbungen konnten bis zum 16. Oktober 2020 eingereicht werden. Derzeit werden die Wettbewerbsbeiträge von der Jury gesichtet, so dass die Nominierten im Dezember bekanntgegeben werden können. 
​
Am 21. Januar 2021 findet die Preisverleihung statt: bitte schon jetzt vormerken!