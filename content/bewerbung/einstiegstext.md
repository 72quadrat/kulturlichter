Die Bewerbungsphase beginnt am 3. August 2020. Wettbewerbsbeiträge können bis zum 16. Oktober 2020 ausschließlich über das Online-Formular auf dieser Website eingereicht werden.

[→ Jetzt hier bewerben!](https://bewerbung.kulturlichter-preis.de/)<br>
[→ Teilnahmebedingungen](%base_url%/?bewerbung/teilnahmebedingungen)<br>
