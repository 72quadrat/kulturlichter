---
Title: Deutscher Preis für kulturelle Bildung
Description: KULTURLICHTER fördert Projekte und Projektideen, die digitale Instrumente in der kulturellen Bildung und der Kulturvermittlung innovativ einsetzen.
opengraphimage: opengraph-standard-teaser.jpg
---
