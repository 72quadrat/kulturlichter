---
title: Datenschutzhinweis
Template: page
---

# Datenschutzhinweis


### 1\. Ansprechpartner / Verantwortliche

Ansprechpartner und Verantwortlicher im Sinne der EU-Datenschutzgrundverordnung (DSGVO) für die Verarbeitung Ihrer personenbezogenen Daten bei Besuch dieser Website ist

Kulturstiftung der Länder – Stiftung des bürgerlichen Rechts (SdbR)<br>
Lützowplatz 9<br>
10785 Berlin<br>
Telefonnummer: 030/8936350<br>
Fax: 030/89363526

Bei Fragen stehen wir Ihnen gern zur Verfügung.

Sie können Ihre Datenschutzanliegen gern auch per E-Mail an unseren Datenschutzbeauftragten unter <datenschutz@kulturstiftung.de> richten oder postalisch an die oben angegebene postalische Adresse (Stichwort: „z. Hd. Datenschutzbeauftragter“).


### 2\. Datenverarbeitung auf unserer Website

####a. Aufruf unserer Website

Bei jeder Nutzung unserer Website erheben wir die Zugriffsdaten, die Ihr Browser automatisch übermittelt, um Ihnen den Besuch der Website zu ermöglichen. Die Zugriffsdaten umfassen insbesondere:

- Datum und Uhrzeit des Zugriffs
- Name der angeforderten Datei
- Webseite, von der aus die Datei angefordert wurde
- Zugriffsstatus (z. B. Datei übertragen, Datei nicht gefunden)
- der von Ihnen verwendete Webbrowser und das Betriebssystem Ihres Gerätes
- die IP-Adresse des anfordernden Gerätes
- Online-Kennungen (z. B. Gerätekennungen, Session-IDs).

Die Datenverarbeitung dieser Zugriffsdaten ist erforderlich, um den Besuch der Website zu ermöglichen und um die dauerhafte Funktionsfähigkeit und Sicherheit unserer Systeme zu gewährleisten. Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. b DSGVO. Aus Datenschutzgründen werden Logfiles bei uns nicht dauerhaft gespeichert oder analysiert.

####[*b. Kontakt*](%base_url%#kontakt)

Wir nutzen keine Kontaktformulare. Sofern Sie zu uns Kontakt aufnehmen wollen bieten wir Ihnen hierfür unsere Emailadresse und Telefonnummer an. Die Daten, die Sie uns auf diesem Wege übermitteln, verwenden wir ausschließlich zur Beantwortung Ihrer Fragen und Wünsche. Rechtsgrundlage der beschriebenen Datenverarbeitung ist Art. 6 Abs. 1 S. 1 lit. b DSGVO. Die erhobenen Daten werden nach vollständiger Bearbeitung Ihrer Anfrage gelöscht, es sei denn, wir benötigen Ihre Anfrage noch zur Erfüllung vertraglicher oder gesetzlicher Pflichten.

####c. Bewerbung für den Kulturlichter Preis

Um eine Bewerbung einsenden zu können, müssen Sie verschiedene Angaben zu Ihrem Projekt machen und uns weitere Dokumente, Medien oder Links, die für die Bewerbung ausschlaggebend sein können durch Upload zur Verfügung stellen.
Wir verwenden nur solche Informationen, die Sie uns zur Verfügung gestellt haben. Wir verarbeiten insbesondere folgende Kategorien Ihrer personenbezogenen Daten im Rahmen des Bewerbungsprozesses:

- Ihre Kontaktdaten wie Name, Emailadresse, Anschrift, Telefonnummer, sowie Ihre Funktion in der sich bewerbenden Einrichtung
- Persönliche Daten, die in Protokolldateien oder Sicherheitsberichten enthalten sind (insbesondere IP-Adresse, Nutzername, Passwort, Ort und Zeitpunkt des Zugriffs auf unser Netzwerk) zum Zwecke des Systemschutzes und der Überwachung und Protokollierung der Nutzung unseres Netzwerkes, um dessen Sicherheit gewährleisten zu können;
Die Bereitstellung Ihrer personenbezogenen Daten ist Voraussetzung für den Bewerbungsprozess und eine Teilnahme am Wettbewerb. Rechtsgrundlage der Verarbeitung oben genannter Daten ist Art. 6 Abs. 1 lit. (b) DSGVO. Die Bewerbungen werden nach einem Jahr gelöscht.

Sollten Sie uns Ihre benötigten Informationen nicht zukommen lassen wollen, können wir Sie für den Preis nicht berücksichtigen. Wir treffen keine automatisierten Entscheidungen.


### 3\. Verwendung eigener Cookies

Für einen Teil unserer Dienste ist es erforderlich, dass wir sogenannte Cookies einsetzen. Ein Cookie ist eine kleine Textdatei, die durch den Browser auf ihrem Gerät gespeichert wird. Cookies werden nicht dazu eingesetzt, um Programme auszuführen oder Viren auf Ihren Computer zu laden. Hauptzweck unserer eigenen Cookies ist vielmehr, ein speziell auf Sie zugeschnittenes Angebot bereitzustellen und die Nutzung unserer Services so zeitsparend wie möglich zu gestalten.
Die meisten Browser sind standardmäßig so eingestellt, dass sie Cookies akzeptieren. Sie können jedoch Ihre Browsereinstellungen so anpassen, dass Cookies abgelehnt oder nur nach vorheriger Einwilligung gespeichert werden. Wenn Sie Cookies ablehnen, können nicht alle unsere Angebote für Sie störungsfrei funktionieren.

Wir verwenden eigene Cookies insbesondere

- zur Speicherung Ihrer Cookie-Präferenzen,
- Zur Log-in Authentifizierung
- Zur Speicherung des Status vom Captcha-Feld im Formular

Wir wollen Ihnen dadurch eine komfortablere und individuellere Nutzung unserer Website ermöglichen. Diese Service-Leistungen beruhen auf unseren vorgenannten berechtigten Interessen, Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DSGVO.

Wir verwenden keine Cookies und vergleichbare Technologien (z. B. Web-Beacons) von Partnern zu Analyse- und Marketingzwecken.

#### Funktionen Sozialer Netzwerke
Diese Website verlinkt auf die Twitter und Instagram Kanäle der Beauftragten der Bundesregierung für Kultur und Medien (BKM), sowie die Twitter, Facebook, YouTube und Instagram Kanäle der Kulturstiftung der Länder.
Die Kulturstiftung der Länder weist ausdrücklich darauf hin, dass beim Besuch der Kanäle in den Sozialen Netzwerken personenbezogene Daten an die jeweiligen Sozialen Netzwerke übermittelt werden. Diese Dienste verarbeiten personenbezogene Daten entsprechend ihrer Geschäftsmodelle, wobei der Ort der Verarbeitung auch außerhalb der Mitgliedstaaten der Europäischen Union liegen kann. Die Kulturstiftung der Länder hat keinen Einfluss auf die von diesen Unternehmen durchgeführte Datenverarbeitung, insbesondere die Datennutzung, Datenübermittlung an Dritte und Dauer der Speicherung.

- Facebook (Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland)
- Facebook-Fanpages auf Grundlage einer Vereinbarung über gemeinsame Verarbeitung personenbezogener Daten
- Datenschutzerklärung: <https://www.facebook.com/about/privacy/>
- Opt-Out: <https://www.facebook.com/settings?tab=ads und http://www.youronlinechoices.com>
- EU-U.S. Privacy Shield: <https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active>.
- Instagram (Facebook Ireland Ltd., 4 Grand Canal Square, Grand Canal Harbour, Dublin 2, Irland)
- Datenschutzerklärung: <https://help.instagram.com/519522125107875>
- Google/ YouTube (Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland)
- Datenschutzerklärung: <https://policies.google.com/privacy>
- Opt-Out: <https://www.google.com/settings/ads>
- EU-U.S. Privacy Shield: <https://www.privacyshield.gov/participant?id=a2zt000000001L5AAI&status=Active>.
- Twitter (Twitter International Company, One Cumberland Place, Fenian Street, Dublin 2, D02 AX07 Irland)
- Datenschutzerklärung: <https://twitter.com/de/privacy>
- Opt-Out: <https://twitter.com/personalization>,
- EU-U.S. Privacy Shield: <https://www.privacyshield.gov/participant?id=a2zt0000000TORzAAO&status=Active>



### 4\. Weitergabe von Daten

Eine Weitergabe der von uns erhobenen Daten erfolgt grundsätzlich nur, wenn:

Sie Ihre nach Art. 6 Abs. 1 S. 1 lit. a DSGVO Ihre ausdrückliche Einwilligung dazu erteilt haben, die Weitergabe nach Art. 6 Abs. 1 S. 1 lit. f DSGVO zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist und kein Grund zur Annahme besteht, dass Sie ein überwiegendes schutzwürdiges Interesse am Unterbleiben der Weitergabe Ihrer Daten haben, wir nach Art. 6 Abs. 1 S. 1 lit. c DSGVO zur Weitergabe gesetzlich verpflichtet sind oder dies gesetzlich zulässig und nach Art. 6 Abs. 1 S. 1 lit. b DSGVO für die Abwicklung von Vertragsverhältnissen mit Ihnen oder für die Durchführung vorvertraglicher Maßnahmen erforderlich ist, die auf Ihre Anfrage hin erfolgen.

Ein Teil der Datenverarbeitung kann durch unsere Dienstleister erfolgen. Neben den in dieser Datenschutzerklärung erwähnten Dienstleistern können hierzu insbesondere Rechenzentren, die unsere Website und Datenbanken speichern, IT-Dienstleister, die unsere Systeme warten, sowie Beratungsunternehmen gehören. Sofern wir Daten an unsere Dienstleister weitergeben, dürfen diese die Daten ausschließlich zur Erfüllung ihrer Aufgaben verwenden. Die Dienstleister wurden von uns sorgfältig ausgewählt und beauftragt. Sie sind vertraglich an unsere Weisungen gebunden, verfügen über geeignete technische und organisatorische Maßnahmen zum Schutz der Rechte der betroffenen Personen und werden von uns regelmäßig kontrolliert.

Darüber hinaus kann eine Weitergabe in Zusammenhang mit behördlichen Anfragen, Gerichtsbeschlüssen und Rechtsverfahren erfolgen, wenn es für die Rechtsverfolgung oder -durchsetzung erforderlich ist.

### 5\. Speicherdauer

Grundsätzlich speichern wir personenbezogene Daten nur solange, wie zur Erfüllung vertraglicher oder gesetzlicher Pflichten erforderlich, zu denen wir die Daten erhoben haben. Danach löschen wir die Daten unverzüglich, es sei denn, wir benötigen die Daten noch bis zum Ablauf der gesetzlichen Verjährungsfrist zu Beweiszwecken für zivilrechtliche Ansprüche oder wegen gesetzlicher Aufbewahrungspflichten. Die Bewerbungsdaten werden ein Jahr nach Ende der Bewerbungszeitraums gelöscht.

### 6\. Ihre Rechte

Ihnen steht jederzeit das Recht zu, Auskunft über die Verarbeitung Ihrer personenbezogenen Daten durch uns zu verlangen. Wir werden Ihnen im Rahmen der Auskunftserteilung die Datenverarbeitung erläutern und eine Übersicht der über Ihre Person gespeicherten Daten zur Verfügung stellen.

Falls bei uns gespeicherte Daten falsch oder nicht mehr aktuell sein sollten, haben Sie das Recht, diese Daten berichtigen zu lassen.

Sie können außerdem die Löschung Ihrer Daten verlangen. Sollte die Löschung aufgrund anderer Rechtsvorschriften ausnahmsweise nicht möglich sein, werden die Daten gesperrt, so dass sie nur noch für diesen gesetzlichen Zweck verfügbar sind.

Sie können die Verarbeitung Ihrer Daten außerdem einschränken lassen, z. B. wenn Sie der Auffassung sind, dass die von uns gespeicherten Daten nicht korrekt sind. Ihnen steht auch das Recht auf Datenübertragbarkeit zu, d. h. dass wir Ihnen auf Wunsch eine digitale Kopie der von Ihnen bereitgestellten personenbezogenen Daten zukommen lassen.

Um Ihre hier beschriebenen Rechte geltenden zu machen, können Sie sich jederzeit an die oben in Ziffer 1 genannten Kontaktdaten wenden.

Zudem haben Sie das Recht der Datenverarbeitung zu widersprechen, die auf Art. 6 Abs. 1 lit. e oder f DSGVO beruht oder der Direktwerbung dient. Sie haben schließlich das Recht sich bei der für uns zuständigen Datenschutzaufsichtsbehörde zu beschweren. Sie können dieses Recht bei einer Aufsichtsbehörde in dem Mitgliedstaat Ihres Aufenthaltsorts, Ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes geltend machen. In Berlin ist die zuständige Aufsichtsbehörde: Berliner Beauftragte für Datenschutz und Informationsfreiheit, Friedrichstr. 219, 10969 Berlin.


Stand: Juli 2020
