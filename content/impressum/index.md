---
title: Impressum
Template: page
---

Impressum
=========
Angaben gem. § 5 TMG

### Betreiber und Kontakt
Kulturstiftung der Länder – Stiftung des bürgerlichen Rechts (SdbR)<br>
Lützowplatz 9<br>
10785 Berlin<br>
Telefonnummer: 030/8936350<br>
Fax: 030/89363526<br>
E-Mail-Adresse: <kontakt@kulturstiftung.de>

### Vertretung
Kulturstiftung der Länder – SdbR wird vertreten durch den Vorstand Prof. Dr. Markus Hilgert

### Projektleitung
Ina von Kunowski<br>
Stabsstelle Kulturelle Bildung der Kulturstiftung der Länder<br>
E-Mail-Adresse: <kontakt@kulturlichter-preis.de>

### Gestaltung
neues handeln AG

### Zuständige Aufsichtsbehörde
Senatsverwaltung für Justiz und Verbraucherschutz<br>
Salzburger Str. 21-25<br>
10825 Berlin

### UMSATZSTEUER-ID:
27/605/51956

---

[![BKM](%assets_url%/BKM_foerderlogo.svg)](https://www.bundesregierung.de/breg-de/bundesregierung/staatsministerin-fuer-kultur-und-medien)
