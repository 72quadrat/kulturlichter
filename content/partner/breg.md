Der kulturelle Reichtum Deutschlands basiert nicht nur auf dem großen, historisch gewachsenen Kulturerbe, sondern auch auf seiner vitalen Kulturszene. Künstlerinnen und Künstler tragen ebenso wie Kreative und Kulturakteure entscheidend zu unserem kulturellen und gesellschaftlichen Leben bei. Ihnen verdanken wir Impulse, Denkanstöße und neue Perspektiven, die eine lebendige Demokratie so dringend braucht.

Zuständig für die Kulturförderung sind in Deutschland vor allem die Länder und Kommunen. Der Bund übernimmt in der Kultur- und Medienpolitik insbesondere Aufgaben von nationaler Bedeutung. Verantwortlich dafür ist die Beauftragte der Bundesregierung für Kultur und Medien (BKM). Ihr Aufgabengebiet ist breit gefächert: Sie unterstützt nicht nur gesamtstaatlich bedeutsame Kultureinrichtungen und -projekte, sie sorgt auch für günstige Rahmenbedingungen in den Bereichen Kultur und Medien und vertritt die kultur- und medienpolitischen Interessen Deutschlands in verschiedenen internationalen Gremien.

Ein besonderes Anliegen der Beauftragten der Bundesregierung für Kultur und Medien ist die breite Vermittlung von Kunst und Kultur. Nur durch sie kann echte Teilhabe möglichst vieler Menschen am sozialen und kulturellen Leben gelingen. „Wir alle spüren, wie wichtig die Kultur für Teilhabe und Zusammenhalt in unserer Gesellschaft ist“, erklärt Monika Grütters. Deshalb   fördert die Staatsministerin für Kultur und Medien kreative, strukturbildende und nachhaltige Projekte der Bildung und Vermittlung.

Weitere Infos gibt es auf [kulturstaatsministerin.de](https://www.bundesregierung.de/breg-de/bundesregierung/staatsministerin-fuer-kultur-und-medien) und in den sozialen Netzwerken Twitter und Instagram.


[![Twitter](%assets_url%/icn-twitter.svg)](https://twitter.com/bundeskultur)
[![Instagram](%assets_url%/icn-instagram.svg)](https://www.instagram.com/bundeskultur/)
