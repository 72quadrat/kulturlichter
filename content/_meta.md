---
Logo: %theme_url%/img/logo.svg
TwitterCard: %assets_url%/twittercard.png
FacebookCard: %assets_url%/facebookcard.png
Tagline: Deutscher Preis für kulturelle Bildung
Copyright: Copyright © 2020 Kulturlichter
PreislistenDachzeile: Im Rahmen des Wettbewerbs werden drei Auszeichnungen vergeben:
PreislistenAbschluss: Eine Jury, die von der Kulturstaatsministerin und der Kulturstiftung der Länder berufen wird, trifft die fachliche Auswahl für den Preis des Bundes und den Preis der Länder.
Social:
    - title: Kulturlichter auf Twitter
      url: http://twitter.com
      icon: twitter
    - title: Kulturlichter auf Instagram
      url: http://instagram.com
      icon: instagram
    - title: Kulturlichter auf Facebook
      url: http://facebook.com
      icon: twitter
---
