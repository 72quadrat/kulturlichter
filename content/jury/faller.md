---
title: Sabine Faller
Template: jury
---
![Sabine Faller](%assets_url%/vitae/Faller_Sabine.jpg)
> Kulturelle Bildung eröffnet einen künstlerischen, kritischen und kreativen Zugang zu unserer heutigen Medien- und Gesellschaftskultur. Als Jurymitglied möchte ich genau jene unterstützen, die diese neuen Kulturtechniken in innovativen Programmen vermitteln, um Wissen zu teilen – partizipativ, kommunikativ und hands-on!

### Sabine Faller
Wissenschaftliche Mitarbeiterin im Zentrum für Kunst und Medien Karlsruhe

[Vita](?jury/vitae/Faller)
