---
title: Prof. Dr. Markus Hilgert
Template: jury
---
![Prof. Dr. Markus Hilgert](%assets_url%/vitae/Hilgert_Markus.jpg)
> Konzepte der kulturellen Bildung im digitalen Bereich können Menschen erreichen, die bislang keinen Zugang zu Kulturangeboten haben. Deshalb ist es mir wichtig, gelungene Beispiele der digitalen Kulturerbe- und Kulturvermittlung sichtbar zu machen und die gemeinsame Arbeit von Kultur- und Bildungseinrichtungen zu fördern.

### Prof. Dr. Markus Hilgert
Generalsekretär der Kulturstiftung der Länder

[Vita](?jury/vitae/Hilgert)
