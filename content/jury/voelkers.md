---
title: Hortensia Völckers
Template: jury
---
![Hortensia Völckers](%assets_url%/vitae/Voelckers_Hortensia.jpg)
> Die Kulturstiftung des Bundes unterstützt in ihrem Förderschwerpunkt Kulturelle Bildung seit 2005 Kultureinrichtungen, neue Vermittlungsformen zu erproben und ein neues Publikum anzusprechen. Mich interessiert daher sehr, welche Konzepte die Kultureinrichtungen umsetzen und welche Fragen sie beschäftigen.

### Hortensia Völckers
Vorstand und Künstlerische Direktorin der Kulturstiftung des Bundes

[Vita](?jury/vitae/Voelckers)
