---
title: Hortensia Völckers
Template: jury-vitae
---
### Hortensia Völckers
*Vorstand und Künstlerische Direktorin der Kulturstiftung des Bundes*

Hortensia Völckers ist seit Gründung im Jahr 2002 Künstlerische Direktorin und Vorstandsmitglied der Kulturstiftung des Bundes in Halle an der Saale. Von 1995 bis 1997 arbeitete sie als Mitglied der künstlerischen Leitung der documenta x in Kassel und gehörte von 1998 bis 2001 dem Direktorium der Wiener Festwochen an. Mit der Kulturstiftung des Bundes entwickelte Hortensia Völckers zahlreiche Programme für den internationalen Kulturaustausch, für die Förderung des Tanzes in Deutschland und zu gesellschaftlich relevanten Themen wie Digitalisierung, ökologische Nachhaltigkeit oder der Zukunft ländlicher Regionen. Neuere und aktuelle Programme wie „TRAFO- Modelle für Kultur im Wandel“, „360°- Fonds für Kulturen in der neuen Stadtgesellschaft“ oder „Fonds Digital“ nehmen aktuelle Herausforderungen der Gesellschaft auf und geben Impulse für Veränderungsprozesse in Kultureinrichtungen.
