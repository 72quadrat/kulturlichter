---
title: Dr. Kathrin Hahne
Template: jury-vitae
---
### Dr. Kathrin Hahne
*Leiterin der Gruppe „Grundsatzfragen der Kulturpolitik, Denkmal- und Kulturschutz“ bei der Beauftragten der Bundesregierung für Kultur und Medien*

Dr. Kathrin Hahne leitet seit dem Jahr 2016 die Gruppe „Grundsatzfragen der Kulturpolitik, Denkmal- und Kulturschutz“ bei der Beauftragten der Bundesregierung für Kultur und Medien (BKM). Seit 2003 hatte sie dort bereits unterschiedliche Funktionen inne: zunächst im Bereich "Rundfunk und Internationale Zusammenarbeit im Medienbereich", später als Referentin im Leitungsstab und als Leiterin der Referate „Förderung ostdeutscher Kultureinrichtungen“, „Denkmalschutz“ und „Baukultur“. Kathrin Hahne ist Juristin und hat ihr Promotionsstudium an den Universitäten Münster, Poitiers und Oxford absolviert.

[Im Bund mit der Kultur. Kultur- und Medienpolitik der Bundesregierung](https://www.bundesregierung.de/resource/blob/973812/424206/02ba8605906154af7772b49f30eb2df9/2016-09-01-neue-kulturbroschuere-data.pdf?download=1), Broschüre der Beauftragten der Bundesregierung für Kultur und Medien