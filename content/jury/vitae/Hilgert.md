---
title: Prof. Dr. Markus Hilgert
Template: jury-vitae
---
### Prof. Dr. Markus Hilgert
*Generalsekretär der Kulturstiftung der Länder*

Prof. Dr. Markus Hilgert ist Altorientalist und seit 1. Juni 2018 Generalsekretär der Kulturstiftung der Länder. Zuvor war er Direktor des Vorderasiatischen Museums im Pergamonmuseum der Stiftung Preußischer Kulturbesitz und lehrte als Professor für Altorientalistik an der Universität Heidelberg. Als Wissenschaftler engagiert sich Markus Hilgert auf den Gebieten der Theorie, Dokumentation und Interpretation von materiellen Kulturgütern, übt zahlreiche ehrenamtliche Funktionen aus und lehrt als Honorarprofessor an den Universitäten Heidelberg und Marburg sowie der Freien Universität Berlin.
