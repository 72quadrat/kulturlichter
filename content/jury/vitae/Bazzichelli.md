---
title: Dr. Tatiana Bazzichelli
Template: jury-vitae
---
### Dr. Tatiana Bazzichelli
*Gründerin und künstlerische Leiterin des Disruption Network Lab*

Tatiana Bazzichelli ist Gründerin und künstlerische Leiterin des Konferenzprogrammes Disruption Network Lab in Berlin. Zuvor war sie Programm- und Konferenzkuratorin beim transmediale Festival, das die kulturelle Transformation aus einer postdigitalen Perspektive kritisch reflektiert. Bazzichellis Arbeit für die transmediale stand im Mittelpunkt ihrer Postdoc-Arbeit am Zentrum für Digitale Kulturen der Leuphana Universität Lüneburg. Sie promovierte 2011 in Informations- und Medienstudien an der Fakultät der Künste der Aarhus Universität (DK). Seit dem Jahr 2019 ist Bazzichelli Mitglied in der Jury des Hauptstadtkulturfonds.
