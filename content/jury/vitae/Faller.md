---
title: Sabine Faller
Template: jury-vitae
---
### Sabine Faller
*Wissenschaftliche Mitarbeiterin im Zentrum für Kunst und Medien Karlsruhe*

Sabine Faller (St.Ex.II) ist wissenschaftliche Mitarbeiterin der Abteilung Museumskommunikation am ZKM | Zentrum für Kunst und Medien in Karlsruhe. Ihr Schwerpunkt liegt in der Konzeption und Umsetzung von Workshops, Projekten und Bildungsprogrammen in den Bereichen Medienkunst und Digitale Bildung. Zudem ist sie Co-Projektleiterin der künstlerischen Stipendienprogramme "Masterclass" und "Kulturakademie" sowie Mitgestalterin des Makerspace "BÄM".
