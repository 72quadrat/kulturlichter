---
title: Mechthild Eickhoff
Template: jury-vitae
---
### Mechthild Eickhoff
*Geschäftsführerin des Fonds Soziokultur*

Mechthild Eickhoff ist Geschäftsführerin des Fonds Soziokultur. Von 2013 bis 2019 hat sie die UZWEI im Dortmunder U aufgebaut: Die zweite Etage im „Zentrum für Kunst und Kreativität“ zielt auf kulturelle Bildung im digitalen Zeitalter in Workshops und Projekten, aber auch in interaktiven, partizipativen Ausstellungen oder etwa im EU-Projekt „smARTplaces“. Zuvor war sie Leiterin des Clusters „Kulturelle Bildung“ bei der Stiftung Mercator und Geschäftsführerin des Bundesverbands der Jugendkunstschulen und Kulturpädagogischen Einrichtungen, bjke.
