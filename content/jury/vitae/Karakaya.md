---
title: Esra Nayeon Karakaya
Template: jury-vitae
---
### Esra Nayeon Karakaya
*Journalistin und Videoproduzentin*

Esra Karakaya ist Videojournalistin und Gründerin ihrer eigenen Youtube-Talkshow BlackRockTalk/KARAKAYA TALK. Die Grimme-nominierte und Grimme-Online-ausgezeichnete Show behandelt Themen von Pop bis Politik und schafft eine Plattform für diejenigen in der deutschen Gesellschaft, die sonst unterrepräsentiert sind und nicht gehört werden. Mit viel Humor und Mut für unangenehme Themen setzt KARAKAYA TALK einen neuen Maßstab für eine respektvolle und empathische Diskussionskultur. Esra Karakaya wurde für ihre Arbeit mehrfach ausgezeichnet unter anderem als „30 unter 30“ vom Zeit Campus Verlag (2019), von der Hertie-Stiftung im Rahmen der #generationgrenzenlos-Kampagne (2019) und mit dem 25 Frauen-Award von Edition F (2019).
