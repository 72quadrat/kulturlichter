---
title: Sabine Jank
Template: jury-vitae
---
### Sabine Jank
*Culturepreneur, szenum.Berlin, Wissenschaftlerin, Beraterin und Coach*

Sabine Jank (Dipl. Des./Dipl. Szen.) ist Kreativdirektorin und Mitbegründerin von szenum.Lab for Participation and Digital Transformation. Im Rahmen ihrer Forschungs– und Arbeitsgebiete Digitale Transformation, Creative Leadership und Partizipative Kultur ist sie sowohl beratend für Kulturinstitutionen tätig, als auch als zertifizierte Coach auf die Professionalisierung von Kulturschaffenden spezialisiert. Sie hält Vorträge und ist Autorin zahlreicher Publikationen zu Digitaler Transformation und Strategien der Partizipation. Seit 2001 lehrt sie an verschiedenen Hochschulen als Dozentin.
