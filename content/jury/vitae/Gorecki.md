---
title: Elisabeth Gorecki-Schöberl
Template: jury-vitae
---
### Elisabeth Gorecki-Schöberl
*Leiterin des Referats „Kulturelle Bildung und Integration, Kultur in den Regionen und Ländlichen Räumen“ bei der Beauftragten der Bundesregierung für Kultur und Medien*

Elisabeth Gorecki-Schöberl leitet das Referat „Kulturelle Bildung und Integration, Kultur in den Regionen und Ländlichen Räumen“ bei der Beauftragten der Bundesregierung für Kultur und Medien (BKM). Zuvor war sie Leiterin des Referats "Internationale Zusammenarbeit" bei der BKM, Referentin für deutsch-französische Beziehungen im Bundeskanzleramt und DAAD-Lektorin an der Universität Paris X/Nanterre. Sie hat Politikwissenschaft und Romanistik an der Universität Heidelberg, Paris III/Sorbonne Nouvelle und am Institut d’Etudes Politiques Paris studiert.

[Im Bund mit der Kultur. Kultur- und Medienpolitik der Bundesregierung](https://www.bundesregierung.de/resource/blob/973812/424206/02ba8605906154af7772b49f30eb2df9/2016-09-01-neue-kulturbroschuere-data.pdf?download=1), Broschüre der Beauftragten der Bundesregierung für Kultur und Medien
