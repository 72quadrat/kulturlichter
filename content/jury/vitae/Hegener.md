---
title: Pia Hegener
Template: jury-vitae
---
### Pia Hegener
*Ministerialrätin, Ministerium für Schule und Bildung des Landes Nordrhein-Westfalen*

Pia Hegener ist seit dem Jahr 2014 im Ministerium für Schule und Bildung des Landes Nordrhein-Westfalen als Referentin zuständig für die Arbeitsschwerpunkte Ganztag und Kulturelle Bildung. Die Tätigkeit umfasst zum Beispiel die Begleitung von Programmen wie „Kulturagenten für kreative Schulen“, „Kreativpotentiale“, der „Arbeitsstelle Kulturelle Bildung NRW“ und zahlreicher weiterer Landesinitiativen zur Stärkung und Verankerung kultureller Bildung in Schulen. Nach dem Abitur und einem Studium der Sonderpädagogik an der Universität Dortmund war Pia Hegener Lehrerin an Förderschulen in Oberhausen, stellvertretende Schulleiterin in Duisburg und Rektorin einer Förderschule in Oberhausen.