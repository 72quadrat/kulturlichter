---
title: Olaf Zimmermann
Template: jury-vitae
---
### Olaf Zimmermann
*Geschäftsführer des Deutschen Kulturrats*

Olaf Zimmermann ist Publizist, Kunsthändler und war Geschäftsführer verschiedener Galerien. 1987 gründete er eine eigene Galerie für zeitgenössische Kunst in Köln und Mönchengladbach. Seit dem Jahr 1997 ist er Geschäftsführer des Deutschen Kulturrates, des Spitzenverbandes der Bundeskulturverbände in Berlin. Zudem ist er Herausgeber und Chefredakteur von Politik & Kultur, der Zeitung des Deutschen Kulturrates.
