---
title: Christoph Deeg
Template: jury-vitae
---
### Christoph Deeg
*Berater und Speaker für die Bereiche Digitale Transformation und Gamification*

Christoph Deeg beschreibt sich selbst als „Gestalter des digital-analogen Lebensraums“. Er ist Berater und Speaker für die Bereiche Digitale Transformation und Gamification. Er berät und begleitet national und international tätige Unternehmen und Organisationen bei der Entwicklung digital-analoger Gesamtstrategien sowie dem strategischen Einsatz von Spielmodellen, zum Beispiel im Kontext digitaler Transformationsprozesse, der Stadtentwicklung oder der Entwicklung digital-analoger Kulturstrategien.
