---
title: Olaf Zimmermann
Template: jury
---
![Olaf Zimmermann](%assets_url%/vitae/Zimmermann_Olaf.jpg)
> KULTURLICHTER – schon der Name begeistert mich. Da muss doch einfach jedem ein Licht aufgehen, wie innovativ kulturelle Bildung ist und welche Impulse hiervon ausgehen. Besonders positiv finde ich, dass Bund und Länder hier zusammenwirken.

### Olaf Zimmermann
Geschäftsführer des Deutschen Kulturrats

[Vita](?jury/vitae/Zimmermann)
