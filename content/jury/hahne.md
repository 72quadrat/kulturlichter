---
title: Dr. Kathrin Hahne
Template: jury
---
![Dr. Kathrin Hahne](%assets_url%/vitae/Hahne_Kathrin.jpg)
> Die kulturelle Teilhabe ist einer der wichtigsten Bausteine für den gesellschaftlichen Zusammenhalt in Deutschland und ein ebenso wichtiger Motor der Integration in unserer Gesellschaft.

### Dr. Kathrin Hahne
Leiterin der Gruppe „Grundsatzfragen der Kulturpolitik, Denkmal- und Kulturschutz“ bei der Beauftragten der Bundesregierung für Kultur und Medien

[Vita](?jury/vitae/Hahne)
