---
title: Dr. Tatiana Bazzichelli
Template: jury
---
![Dr. Tatiana Bazzichelli](%assets_url%/vitae/Bazzichelli_Tatiana.jpg)

### Dr. Tatiana Bazzichelli
Gründerin und künstlerische Leiterin des Disruption Network Lab

[Vita](?jury/vitae/Bazzichelli)

> Die Jury sollte neue Möglichkeiten eröffnen und neue Ideen aus der Szene der hybriden Kunst und Technologie sowie der digitalen Kultur wertschätzen. Das Kunstwerk ist oft Teil eines sozialen und relationalen Prozesses. Der Preis macht Projekte sichtbar, die digitale Technologien nutzen, um zu einer bessere Gesellschaft beizutragen.
