---
title: Pia Hegener
Template: jury
---
![Pia Hegener](%assets_url%/vitae/Hegener_Pia.jpg)
> Gerade jetzt ist es unverzichtbar, das Potential kultureller Bildung zu stärken und innovative Ideen der Vermittlung zu würdigen und zu verbreiten, damit sie langfristig wirksam werden können. Daher engagiere ich mich gerne für KULTURLICHTER!

### Pia Hegener
Ministerialrätin, Ministerium für Schule und Bildung des Landes Nordrhein-Westfalen

[Vita](?jury/vitae/Hegener)
