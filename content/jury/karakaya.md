---
title: Esra Nayeon Karakaya
Template: jury
---
![Esra Nayeon Karakaya](%assets_url%/vitae/Karakaya_Esra.jpg)
> Progressive, kluge und wirkungsvolle Projekte in der kulturellen Bildung gibt es so einige - aber die öffentliche Anerkennung und Förderung bleibt dennoch in vielen Fällen aus. Umso mehr freue ich mich, mit dieser Gelegenheit Projekten Wertschätzung entgegenzubringen.“

### Esra Karakaya
Journalistin und Videoproduzentin<br/>

<small>Foto: Meklit Tsige Fekadu</small>

[Vita](?jury/vitae/Karakaya)
