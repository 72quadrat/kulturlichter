---
title: Elisabeth Gorecki-Schöberl
Template: jury
---
![Elisabeth Gorecki-Schöberl](%assets_url%/vitae/Gorecki-Schoeberl_Elisabeth.jpg)
> Die Begegnung und Auseinandersetzung mit Kunst und Kultur ermöglichen einen Zugang zu Geschichte, zu den Traditionen, Werten und kulturellen Leistungen in Deutschland, Europa und der Welt.

### Elisabeth Gorecki-Schöberl
Leiterin des Referats „Kulturelle Bildung und Integration, Kultur in den Regionen und Ländlichen Räumen“ bei der Beauftragten der Bundesregierung für Kultur und Medien

[Vita](?jury/vitae/Gorecki)
