---
title: Christoph Deeg
Template: jury
---
![Christoph Deeg](%assets_url%/vitae/Deeg_Christoph.jpg)

### Christoph Deeg
Berater und Speaker für die Bereiche Digitale Transformation und Gamification

[Vita](?jury/vitae/Deeg)

> Das Internet ist menschlich.