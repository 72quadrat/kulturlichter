---
title: Mechthild Eickhoff
Template: jury
---
![Mechthild Eickhoff](%assets_url%/vitae/Eickhoff_Mechthild.jpg)
> Kulturelle Bildung ist kein Add-On für Kultur, sondern ein professionelles Setting für Handlungskompetenz und lebensnotwendiges, sinnliches Bewusstsein. Das wird in der Debatte um Kultur oft vergessen. Daher ist es sehr gut, dass KULTURLICHTER die Bedeutung kultureller Bildung auf Bundes-, Landes- und Gesellschaftsebene deutlich macht.

### Mechthild Eickhoff
Geschäftsführerin des Fonds Soziokultur

[Vita](?jury/vitae/Eickhoff)
