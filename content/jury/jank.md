---
title: Sabine Jank
Template: jury
---
![Sabine Jank](%assets_url%/vitae/Jank_Sabine.jpg)
> Ich engagiere mich für KULTURLICHTER, weil der Preis Projekte mit progressiven Perspektiven zur kulturellen Bildung unterstützt. Innovationsorientierung und die Möglichkeit zur Weiterentwicklung der geförderten Projekte machen diese zu wichtigen Impulsgebern und Inspirationsquellen für diverse Akteure.

### Sabine Jank
Culturepreneur, szenum.Berlin, Wissenschaftlerin, Beraterin und Coach

[Vita](?jury/vitae/Jank)
