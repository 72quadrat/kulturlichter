$( document ).ready(function() {

  /////////////////////////////////////////////////////
  // waypoint animation

  // Kopf-Visual
  var waypoint1 = $('#waypoint1').waypoint({
    handler: function(direction) {
      //console.log('Wegpunkt 1 '+ direction);
      if (direction === 'up') {
        $('.visual-a').removeClass('newposition');
      } else {
        $('.visual-a').addClass('newposition');
      }
    }
  });

  // Visuals links und rechts
  var waypoint2 = $('#waypoint2').waypoint({
    handler: function(direction) {
      //console.log('Wegpunkt 2 '+ direction);
      if (direction === 'up') {
        $('.visual-b').removeClass('newposition');
        $('.visual-c').removeClass('newposition');
      } else {
        $('.visual-b').addClass('newposition');
        $('.visual-c').addClass('newposition');
      }
    }
  });

  // Jury wird erreicht
  var jury = new Waypoint.Inview({
    element: $('#jury')[0],
    enter: function(direction) {
      console.log('Enter triggered with direction ' + direction);
      $('.bewerbung-jury-background').addClass('inview');
    },
    exit: function(direction) {
      console.log('Exit triggered with direction ' + direction);
      $('.bewerbung-jury-background').removeClass('inview');
    }
  })

  // Typo kommt auf die Seite
  var typoani = $('.variablefont-ani').waypoint({
    handler: function(direction) {
      //console.log('typoani '+ direction);
      if (direction === 'up') {
        $(this.element).removeClass('active');
      } else {
        $(this.element).addClass('active');
      }
    },
    offset: '75%'
  });

  /////////////////////////////////////////////////////
  // Accordion
  $('.accordion a').click(function(e) {
    // stop standard functionality
    e.preventDefault();
    // handle paragraph;
    $(this).parent().next().toggleClass('open');

  });

  /////////////////////////////////////////////////////
  // Jury PopUps
  $('.jury-mitglied.carousel-cell a').magnificPopup({
    type: 'ajax'
  });

  /////////////////////////////////////////////////////
  // Animation beim Laden der Seite
  $('.visual-a').addClass('loadani');
  window.onload = function(e) {
    setTimeout(function(){
      //console.log('ready');
      $('.visual-a').removeClass('loadani');
    }, 1000);
  };

  /////////////////////////////////////////////////////
  // Animation per Maus- oder Scoll-Position
  // Scroll:
  $(window).scroll(function(){
    var ScrollPos = $(document).scrollTop();
    // Haupt-Titel
    var SublineTargetx = $('.subline').offset().top;
    var SublinePercentage = Math.round(ScrollPos / SublineTargetx  * 100);
    var SublineWeight = Math.round(SublinePercentage / 100 * 900);
    //console.log(ScrollPos+" "+SublineTargetx+ " "+SublinePercentage+" "+SublineWeight);
    $('.kulturlichter-title').css("font-variation-settings", "'wght' "+SublineWeight*1.6);
    // Bewerbungs-text
    var BewerbungTargetx = $('#bewerbung').offset().top;
    var BewerbungPercentage = Math.round((ScrollPos - BewerbungTargetx / 1.5) / BewerbungTargetx * 100);
    var BewerbungWeight = Math.round(BewerbungPercentage / 100 * 900);
    //console.log("scroll:"+ScrollPos+" Target"+BewerbungTargetx+ " Percentage:"+BewerbungPercentage+" Weight:"+BewerbungWeight);
    $('#bewerbung p').css("font-variation-settings", "'wght' "+BewerbungWeight*1.6);

  });
  // Maus:
  var mouseani = document.getElementById('waypoint1');
  mouseani.onmousemove = function(e) {
    var x = e.pageX - this.offsetLeft;
    var y = e.pageY - this.offsetTop;
    var distance = x/e.pageX;
    //console.log(x);
    $('.kulturlichter-title').css("font-variation-settings", "'wght' "+x);
  };

});
