$( document ).ready(function() {

  /////////////////////////////////////////////////////
  // Navigation und Burger

  $('.b-menu, .b-brand').click(function() {
    //console.log( "Handler for .click() called." );
    $('body, .b-container, .mainnav').toggleClass('open');
  });

  $('.home .mainnav a.scrolllink').click(function(e) {
    // stop standard functionality
    e.preventDefault();
    // close burgermenu
    $('body, .b-container, .mainnav').removeClass('open');
    // get targtet an scroll smooth down
    var target = this.hash;
    var $target = $(target);
    $('html, body').stop().animate({
     'scrollTop': $target.offset().top
   }, 1300, 'swing', function () {
     window.location.hash = target;
    });
  });


});
