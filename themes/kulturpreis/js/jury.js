$( document ).ready(function() {

  /////////////////////////////////////////////////////
  // Jury Animation
  // Kopf-Visual

  // Einmal via CSS die Jurymitglieder ausblenden
  $('.jury-mitglied').addClass('off');

  // Dann bei erreichen ein
  var jurymitglied = $('.jury-mitglied').waypoint({
    handler: function(direction) {
      console.log(this.element.id);
      if (direction === 'up') {
        $(this.element).addClass('off');
      } else {
        $(this.element).removeClass('off');
      }
    }
  });

});
